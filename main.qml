import QtQuick 2.15
import QtQuick.Window 2.15
import CustomItems 1.0

Window {
    visible: true
    width: 640
    height: 480

    TextureItem {
        id: textureItem
        objectName: "textureItem"
        anchors.fill: parent
    }
}
