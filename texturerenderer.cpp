#include "texturerenderer.h"
#include "textureitem.h"

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <QOpenGLContext>
#include <QOpenGLShader>

#include <QtDebug>
#include <cassert>

#include <unistd.h>

#include "dmabuf_info.h"

//===========================================
// デスクトップで動かす場合はqtbase5-gles-devが必要
//===========================================

static void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
    GLenum err = glGetError();
    if (err != GL_NO_ERROR)
    {
        printf("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
        abort();
    }
}

#define GL_CHECK(stmt) do {                             \
        stmt;                                           \
        CheckOpenGLError(#stmt, __FILE__, __LINE__);    \
    } while (0)

constexpr int PROGRAM_VERTEX_ATTRIBUTE = 0;
constexpr int PROGRAM_TEXCOORD_ATTRIBUTE = 1;

EGLImageKHR create_egl_image(EGLDisplay display, const DmabufInfo& dmabuf)
{

    // EGLの拡張機能を取得
    PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR =
        (PFNEGLCREATEIMAGEKHRPROC)eglGetProcAddress("eglCreateImageKHR");
    assert(eglCreateImageKHR);

    // DMA-BUF fdからEGLImageを作成
    EGLint attrs[] = {
        EGL_WIDTH, dmabuf.width,
        EGL_HEIGHT, dmabuf.height,
        EGL_LINUX_DRM_FOURCC_EXT, dmabuf.format,
        EGL_DMA_BUF_PLANE0_FD_EXT, dmabuf.fd0,
        EGL_DMA_BUF_PLANE0_OFFSET_EXT, dmabuf.offset0,
        EGL_DMA_BUF_PLANE0_PITCH_EXT, dmabuf.stride0,
        EGL_DMA_BUF_PLANE1_FD_EXT, dmabuf.fd1,
        EGL_DMA_BUF_PLANE1_OFFSET_EXT, dmabuf.offset1,
        EGL_DMA_BUF_PLANE1_PITCH_EXT, dmabuf.stride1,
        EGL_NONE
    };

    EGLImageKHR eglImage = eglCreateImageKHR(display, EGL_NO_CONTEXT,
                                             EGL_LINUX_DMA_BUF_EXT,
                                             NULL, attrs);
    if (eglImage == EGL_NO_IMAGE_KHR) {
        qFatal("eglCreateImageKHR failed");
        abort();
    }

    {
        GLenum err = eglGetError();
        if (err != EGL_SUCCESS)
        {
            qFatal("eglCreateImageKHR error %08x", err);
            abort();
        }
    }

    return eglImage;
}

void create_texture(GLuint texture_id, EGLImageKHR egl_image)
{
    PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES =
        (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC)eglGetProcAddress("glEGLImageTargetTexture2DOES");
    assert(glEGLImageTargetTexture2DOES);

    GL_CHECK(glActiveTexture(GL_TEXTURE0));

    GL_CHECK(glBindTexture(GL_TEXTURE_EXTERNAL_OES, texture_id));
    GL_CHECK(glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, egl_image));
}


TextureRenderer::TextureRenderer()
{
    QOpenGLShader* vshader = new QOpenGLShader(QOpenGLShader::Vertex, nullptr);
    const char *vsrc =
        "attribute highp vec4 vertex;\n"
        "attribute mediump vec4 texCoord;\n"
        "varying mediump vec4 texc;\n"
        "void main(void)\n"
        "{\n"
        "    gl_Position = vertex;\n"
        "    texc = texCoord;\n"
        "}\n";
    vshader->compileSourceCode(vsrc);
    assert(vshader->isCompiled());

    QOpenGLShader* fshader = new QOpenGLShader(QOpenGLShader::Fragment, nullptr);
    const char *fsrc =
        "#extension GL_OES_EGL_image_external : require \n"
        "uniform samplerExternalOES texture;\n"
        "varying mediump vec4 texc;\n"
        "void main(void)\n"
        "{\n"
        "    gl_FragColor = texture2D(texture, texc.st);\n"
        "}\n";
    fshader->compileSourceCode(fsrc);
    assert(fshader->isCompiled());

    program_ = std::make_unique<QOpenGLShaderProgram>();
    program_->addShader(vshader);
    program_->addShader(fshader);
    program_->link();
    assert(program_->isLinked());

    // 頂点データ: 2つの三角形で矩形を形成
    float vertices[] = {
        // 位置      // テクスチャ座標
        -1.0f,  1.0f,  0.0f, 1.0f,
        -1.0f, -1.0f,  0.0f, 0.0f,
        1.0f, -1.0f,  1.0f, 0.0f,

        1.0f, -1.0f,  1.0f, 0.0f,
        1.0f,  1.0f,  1.0f, 1.0f,
        -1.0f,  1.0f,  0.0f, 1.0f
    };

    // 2. VAO、VBOの設定
    vao_ = new QOpenGLVertexArrayObject;
    vao_->create();
    vao_->bind();

    vbo_ = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    vbo_->create();
    vbo_->bind();
    vbo_->allocate(vertices, sizeof(vertices));

    // 頂点属性の指定
    program_->enableAttributeArray("vertex");
    program_->setAttributeBuffer("vertex", GL_FLOAT, 0, 2, 4 * sizeof(float));
    program_->enableAttributeArray("texCoord");
    program_->setAttributeBuffer("texCoord", GL_FLOAT, 2 * sizeof(float), 2, 4 * sizeof(float));

    vbo_->release();
    vao_->release();

    GL_CHECK(glGenTextures(1, &texture_id_));
}

void TextureRenderer::render()
{
    auto parent = dynamic_cast<TextureItem*>(parent_);
    static void* p = nullptr;

    // NOTE: EGLDisplayは必ずrender threadで作成したものを使用する。
    EGLDisplay display = eglGetCurrentDisplay();
    assert(display);

    PFNEGLDESTROYIMAGEKHRPROC eglDestroyImageKHR =
        (PFNEGLDESTROYIMAGEKHRPROC)eglGetProcAddress("eglDestroyImageKHR");
    assert(eglDestroyImageKHR);


    if (p != parent) {
        qDebug("render parent: %p", parent);
        p = parent;
    }

    if (!parent) {
        update();
        return;
    }


    DmabufInfo dmabuf = parent->get_dmabuf();
    if (dmabuf.fd0 == 0 || dmabuf.fd1 == 0) {
        update();
        return;
    }

    if (dmabuf_.fd0 != dmabuf.fd0 || dmabuf_.fd1 != dmabuf.fd1) {

        if (dmabuf_.fd0) close(dmabuf_.fd0);
        if (dmabuf_.fd1) close(dmabuf_.fd1);

        dmabuf_ = dmabuf;

        if (egl_image_) {
            eglDestroyImageKHR(display, egl_image_);
        }

        egl_image_ = create_egl_image(display, dmabuf_);
        create_texture(texture_id_, egl_image_);
    }

    program_->bind();
    GL_CHECK(glActiveTexture(GL_TEXTURE0));

    GL_CHECK(glBindTexture(GL_TEXTURE_EXTERNAL_OES, texture_id_));

    GL_CHECK(glViewport(0, 0,
                        framebufferObject()->width(),
                        framebufferObject()->height()));

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // NOTE: GL_DEPTH_BUFFER_BITがないと表示されない
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLuint location = program_->uniformLocation("texture");
    program_->setUniformValue(location, 0);  // GL_TEXTURE0

    vao_->bind();
    GL_CHECK(glDrawArrays(GL_TRIANGLES, 0, 6));
    vao_->release();

    program_->release();

    update();
}

 void TextureRenderer::synchronize(QQuickFramebufferObject* item)
{
    qDebug() << "TextureRenderer::synchronize()";
    parent_ = item;
    printf("!!! parent = %p\n", parent_);
    printf("render this = %p\n", this);
    QQuickFramebufferObject::Renderer::synchronize(item);
}
