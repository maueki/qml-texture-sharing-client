#pragma once

#include <QObject>
#include <wayland-client.h>

#include "qwayland-texture-sharing.h"
#include "dmabuf_info.h"

class QGuiApplication;

class TsClient : public QObject {

    Q_OBJECT

public:
    TsClient(QGuiApplication& app);

private:
    static void registry_handle_global(void *data,
                                       struct wl_registry *registry,
                                       uint32_t name,
                                       const char *interface,
                                       uint32_t version);

    static void registry_handle_global_remove(void *data,
                                       struct wl_registry *registry,
                                       uint32_t name);

    static const wl_registry_listener registry_listener;

    wl_display* display_;
    QtWayland::texture_sharing *texture_sharing_ = nullptr;

signals:
    void dmabuf_received(DmabufInfo dmabuf);
};
