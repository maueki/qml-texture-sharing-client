#pragma once

#include <QObject>
#include <QQuickFramebufferObject>

#include "texturerenderer.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <atomic>
#include "dmabuf_info.h"

class TextureItem : public QQuickFramebufferObject
{
    Q_OBJECT

    QML_ELEMENT

public:

    TextureItem(const TextureItem&)=delete;
    TextureItem(TextureItem&&)=delete;
    TextureItem& operator=(const TextureItem&)=delete;
    TextureItem& operator=(TextureItem&&)=delete;

    TextureItem(QQuickItem* parent=nullptr);


    Renderer *createRenderer() const override {
        auto renderer = new TextureRenderer();
        printf("!!! createRenderer: %p\n", renderer);
        return renderer;
    }

    void set_dmabuf(const DmabufInfo& dmabuf);

    DmabufInfo get_dmabuf() {
        return dmabuf_;
    }

private:
    std::atomic<DmabufInfo> dmabuf_ = {};
};
