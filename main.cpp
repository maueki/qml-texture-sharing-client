#include <memory>

#include <QObject>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "tsclient.h"
#include "textureitem.h"
#include "dmabuf_info.h"

int main(int argc, char *argv[])
{
    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    // EGLディスプレイを初期化
    EGLint majorVersion, minorVersion;
    EGLBoolean result = eglInitialize(display, &majorVersion, &minorVersion);
    if (result == EGL_FALSE) {
        fprintf(stderr, "Failed to call eglInitialize()\n");
        return -1;
    }

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    auto tsclient = std::make_unique<TsClient>(app);

    QObject::connect(tsclient.get(), &TsClient::dmabuf_received,
                     [&engine](const DmabufInfo& dmabuf) {
                         QObject* root = engine.rootObjects().first();
                         auto obj = root->findChild<QObject*>("textureItem");
                         if (!obj) {
                             return;
                         }

                         auto texture_item = dynamic_cast<TextureItem*>(obj);
                         texture_item->set_dmabuf(dmabuf);
                     });

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
