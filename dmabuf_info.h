#pragma once

#include <cstdint>

struct DmabufInfo {
    int32_t width;
    int32_t height;
    uint32_t format;
    int32_t fd0; uint32_t offset0; uint32_t stride0;
    int32_t fd1; uint32_t offset1; uint32_t stride1;
};
