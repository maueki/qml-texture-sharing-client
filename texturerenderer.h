#pragma once
#include <memory>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <QQuickFramebufferObject>
#include <QOpenGLFramebufferObjectFormat>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>

#include <QOpenGLTexture>

#include "dmabuf_info.h"

class TextureRenderer : public QQuickFramebufferObject::Renderer
{
public:
    TextureRenderer();
    virtual ~TextureRenderer(){}

    void render() override;

    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size) override {
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
        format.setSamples(4);
        return new QOpenGLFramebufferObject(size, format);
    }

    void synchronize(QQuickFramebufferObject* item) override;
private:
    std::unique_ptr<QOpenGLShaderProgram> program_;
    std::unique_ptr<QOpenGLBuffer> vertex_buffer_;
    std::unique_ptr<QOpenGLBuffer> texcoord_buffer_;

    QOpenGLVertexArrayObject *vao_ = nullptr;
    QOpenGLBuffer *vbo_ = nullptr;

    QQuickFramebufferObject* parent_ = nullptr;

    GLuint texture_id_ = 0;

    EGLImageKHR egl_image_ = nullptr;
    DmabufInfo dmabuf_;
};
