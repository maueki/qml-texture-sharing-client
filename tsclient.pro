QT += quick \
  gui-private

CONFIG += \
    debug \
    es2 \
    qmltypes \
    c++17 \
    wayland-scanner \
    link_pkgconfig

QML_IMPORT_NAME=CustomItems
QML_IMPORT_MAJOR_VERSION=1

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        tsclient.cpp \
        texturerenderer.cpp \
        textureitem.cpp

RESOURCES += qml.qrc

WAYLANDCLIENTSOURCES += texture-sharing.xml

PKGCONFIG += \
    wayland-client

HEADERS += \
    tsclient.h \
    texturerenderer.h \
    textureitem.h

LIBS += -ldrm -lGL -lEGL -latomic
