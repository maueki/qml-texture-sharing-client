#include "textureitem.h"

#include <QQuickWindow>

TextureItem::TextureItem(QQuickItem* parent):
    QQuickFramebufferObject(parent)
{
}

void TextureItem::set_dmabuf(const DmabufInfo& dmabuf)
{
    dmabuf_ = dmabuf;
}
