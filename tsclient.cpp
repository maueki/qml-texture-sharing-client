
#include "tsclient.h"

#include <functional>
#include <QtDebug>
#include <QGuiApplication>
#include <qpa/qplatformnativeinterface.h>

#include "qwayland-texture-sharing.h"

#include "dmabuf_info.h"

class TextureSharingImpl : public QtWayland::texture_sharing {
public:
    TextureSharingImpl(struct ::wl_registry *registry, int id, int version,
                       std::function<void (const DmabufInfo&)> cb) :
        texture_sharing(registry, id, version),
        dmabuf_info_callback_(cb)
        {}

    void texture_sharing_dmabuf_fd(int32_t width, int32_t height, uint32_t format,
                                   int32_t fd0, uint32_t offset0, uint32_t stride0,
                                   int32_t fd1, uint32_t offset1, uint32_t stride1) override {

        DmabufInfo buf_info{width, height, format,
                            fd0, offset0, stride0,
                            fd0, offset0, stride0};

        dmabuf_info_callback_(buf_info);
    }

private:
    std::function<void (const DmabufInfo&)> dmabuf_info_callback_;
};

void TsClient::registry_handle_global(void *data,
                                          struct wl_registry *registry,
                                          uint32_t name,
                                          const char *interface,
                                          uint32_t version)
{
    auto client = reinterpret_cast<TsClient*>(data);

    if (!strcmp(interface, "texture_sharing"))
    {
        qDebug("bind texture_sharing");

        client->texture_sharing_ =
            new TextureSharingImpl(registry, name, version,
                                   [client](const DmabufInfo& dmabuf) {
                                       emit client->dmabuf_received(dmabuf);
                                   });
    }
}

void TsClient::registry_handle_global_remove(void *data,
                                                  struct wl_registry *registry,
                                                  uint32_t name)
{
    (void)data, (void)registry, (void)name;
}

const wl_registry_listener TsClient::registry_listener = {
    TsClient::registry_handle_global,
    TsClient::registry_handle_global_remove
};

TsClient::TsClient(QGuiApplication& app) {

    QPlatformNativeInterface *native = QGuiApplication::platformNativeInterface();
    display_ = static_cast<struct wl_display*>(native->nativeResourceForIntegration("wl_display"));

    assert(display_);

    wl_registry *registry = wl_display_get_registry(display_);
    assert(registry);

    wl_registry_add_listener(registry, &registry_listener, this);
}
